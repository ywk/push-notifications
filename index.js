var keys = require('./keys')
const settings = {
  gcm: {
    id: keys.serverKey
  }
}

const destinationToken = keys.deviceKey

const PushNotifications = new require('node-pushnotifications')
const push = new PushNotifications(settings)

const data = {
  // custom: {
  //   custom_notification: {
  //       body: 'test body 22',
  //       title: 'test title',
  //       priority: 'high',
  //       show_in_foreground: true,
  //       click_action: 'fcm.flair.notification',
  //       data: {
  //         a:'aa',
  //         b:'bb'
  //       }
  //   }
  // }
  data: {
    title: 'New push notification', // REQUIRED
    body: 'Powered by AppFeel', // REQUIRED
    alert: 'Here is a new message',
    priority: 'high', // gcm, apn. Supported values are 'high' or 'normal' (gcm). Will be translated to 10 and 5 for apn. Defaults to 'high'
    topic: 'sg.flair.ios' // apn and gcm for ios
  }
}
/**
 * 1. Opening immediately
 * 2. Opening before token 
 * 3. Opening after token received and no more activity
 */

push.send(destinationToken, data)
.then((results) => {
  console.log('Success in sending to GCM/FCM Server')
  if (results[0].success === 1 && results[0].failure === 0) {
    console.log('Success in sending to device')
  } else {
    console.log('Error in sending to device')
  }
})
.catch((err) => {
  console.log('Error in sending to GCM/FCM Server')
  console.log(err)
});